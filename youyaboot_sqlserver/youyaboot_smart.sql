-- 跟youyaboot.sql一致，只不过这里的格式更美化些，注意暂无字段注释 第一句有可能需要手动先执行 拷贝到命令行即可执行
create DATABASE youyaboot;
-- 下面的语句等 create DATABASE youyaboot; 执行成功后再执行--
use youyaboot;
CREATE TABLE youyaboot.dbo.dict
(
  id bigint PRIMARY KEY NOT NULL IDENTITY,
  dict_category varchar(50) DEFAULT '',
  code varchar(50) DEFAULT '',
  name varchar(300) DEFAULT '',
  dict_description text,
  parent_id bigint,
  create_time datetime,
  update_time datetime,
  order_no tinyint
);

CREATE TABLE youyaboot.dbo.goods
(
  id bigint PRIMARY KEY NOT NULL IDENTITY,
  goods_name varchar(200) DEFAULT '',
  publish_status char(1) DEFAULT 0,
  goods_status tinyint DEFAULT 0,
  price decimal(12,2) DEFAULT 0,
  store_count int DEFAULT 0,
  short_brief text,
  goods_description ntext,
  create_time datetime,
  update_time datetime,
  img_src varchar(200) DEFAULT '',
  goods_category_id bigint
);
SET IDENTITY_INSERT youyaboot.dbo.goods ON
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (4, '测试商品0', '0', null, null, 0, '', '', '2018-09-21 15:13:34.000', null, '', null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (5, '测试商品2', null, null, null, null, null, null, '2018-09-26 16:34:10.623', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (6, '测试商品3', null, null, null, null, null, null, '2018-09-26 16:34:10.623', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (7, '测试商品4', null, null, null, null, null, null, '2018-09-26 16:34:10.663', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (8, '测试商品51', '1', 0, 1.00, 1, '1', '1', '2018-09-26 16:46:23.000', '2018-09-26 16:46:25.000', '1', 14);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (9, '测试商品0', null, null, null, null, null, null, '2018-09-26 16:34:46.257', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (10, '测试商品1', null, null, null, null, null, null, '2018-09-26 16:34:58.047', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (11, '测试商品2', '0', 0, 0.00, 0, null, null, '2018-09-26 16:34:58.757', null, '', null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (12, '测试商品0', null, null, null, null, null, null, '2018-09-26 16:35:35.697', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (13, '测试商品1', null, null, null, null, null, null, '2018-09-26 16:35:36.297', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (14, '测试商品2', '0', 0, 0.00, 0, null, null, '2018-09-26 16:35:36.300', null, '', null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (15, '测试商品3', null, null, null, null, null, null, '2018-09-26 16:35:36.310', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (16, '测试商品4', null, null, null, null, null, null, '2018-09-26 16:35:36.310', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (17, '测试商品0', null, null, null, null, null, null, '2018-09-26 16:36:50.353', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (18, '测试商品1', null, null, null, null, null, null, '2018-09-26 16:36:50.977', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (19, '测试商品2', '0', 0, 0.00, 0, null, null, '2018-09-26 16:36:54.027', null, '', null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (20, '测试商品3', null, null, null, null, null, null, '2018-09-26 16:36:54.877', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (21, '测试商品4', null, null, null, null, null, null, '2018-09-26 16:36:54.877', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (22, '测试商品0', null, null, null, null, null, null, '2018-09-26 16:40:00.147', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (23, '测试商品1', null, null, null, null, null, null, '2018-09-26 16:40:00.783', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (24, '测试商品2', '0', 0, 0.00, 0, null, null, '2018-09-26 16:40:03.760', null, '', null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (25, '测试商品3', null, null, null, null, null, null, '2018-09-26 16:40:04.457', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (26, '测试商品4', null, null, null, null, null, null, '2018-09-26 16:40:04.457', null, null, null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (27, '测试商品5', '0', 0, 0.00, 0, null, null, '2018-09-26 16:40:05.577', null, '', null);
INSERT INTO youyaboot.dbo.goods (id, goods_name, publish_status, goods_status, price, store_count, short_brief, goods_description, create_time, update_time, img_src, goods_category_id) VALUES (28, '测试商品61', '1', 0, 0.00, 12, '', '', '2018-09-26 16:40:05.000', null, '', 14);
SET IDENTITY_INSERT youyaboot.dbo.goods OFF

CREATE TABLE youyaboot.dbo.goods_category
(
  id bigint PRIMARY KEY NOT NULL IDENTITY,
  name varchar(50) DEFAULT '',
  keyword varchar(50) DEFAULT ''
);
SET IDENTITY_INSERT youyaboot.dbo.goods_category ON
INSERT INTO youyaboot.dbo.goods_category (id, name, keyword) VALUES (4, '356', '2018-10-01 17:52:30');
INSERT INTO youyaboot.dbo.goods_category (id, name, keyword) VALUES (5, 'b', '');
INSERT INTO youyaboot.dbo.goods_category (id, name, keyword) VALUES (6, 'c', '');
INSERT INTO youyaboot.dbo.goods_category (id, name, keyword) VALUES (7, 'd', '');
INSERT INTO youyaboot.dbo.goods_category (id, name, keyword) VALUES (8, 'e', '');
INSERT INTO youyaboot.dbo.goods_category (id, name, keyword) VALUES (9, 'f', '');
INSERT INTO youyaboot.dbo.goods_category (id, name, keyword) VALUES (10, 'g', '');
INSERT INTO youyaboot.dbo.goods_category (id, name, keyword) VALUES (11, 'h', '');
INSERT INTO youyaboot.dbo.goods_category (id, name, keyword) VALUES (12, 'i', '');
INSERT INTO youyaboot.dbo.goods_category (id, name, keyword) VALUES (13, 'j', '');
INSERT INTO youyaboot.dbo.goods_category (id, name, keyword) VALUES (14, 'k', '');
INSERT INTO youyaboot.dbo.goods_category (id, name, keyword) VALUES (15, '223', '2018-10-01 15:49:15');
SET IDENTITY_INSERT youyaboot.dbo.goods_category OFF

CREATE TABLE youyaboot.dbo.goods_file
(
  file_id bigint PRIMARY KEY NOT NULL IDENTITY,
  file_src varchar(50) DEFAULT '',
  goods_id bigint
);

CREATE TABLE youyaboot.dbo.goods_img
(
  img_id bigint PRIMARY KEY NOT NULL IDENTITY,
  img_src varchar(100) DEFAULT '',
  goods_id bigint
);

CREATE TABLE youyaboot.dbo.sys_admin_user
(
  id bigint PRIMARY KEY NOT NULL IDENTITY,
  username varchar(20),
  password varchar(50),
  real_name varchar(25),
  email varchar(25),
  telephone varchar(20),
  mobile_phone varchar(20),
  address varchar(100),
  create_time datetime,
  update_time datetime,
  role_id bigint,
  account_non_expired bit,
  account_non_locked bit,
  credentials_non_expired bit,
  enabled bit
);
CREATE UNIQUE INDEX sys_admin_user_username_uindex ON youyaboot.dbo.sys_admin_user (username);
SET IDENTITY_INSERT youyaboot.dbo.sys_admin_user ON
INSERT INTO youyaboot.dbo.sys_admin_user (id, username, password, real_name, email, telephone, mobile_phone, address, create_time, update_time, role_id, account_non_expired, account_non_locked, credentials_non_expired, enabled) VALUES (3, 'admin', '21232f297a57a5a743894a0e4a801fc3', '管理员', '', '', '', '', '2018-07-30 00:00:00.000', '2018-07-30 00:00:00.000', 1, 0, 0, 0, 1);
INSERT INTO youyaboot.dbo.sys_admin_user (id, username, password, real_name, email, telephone, mobile_phone, address, create_time, update_time, role_id, account_non_expired, account_non_locked, credentials_non_expired, enabled) VALUES (4, 'test', '098f6bcd4621d373cade4e832627b4f6', '测试人员', '', '', '', '', '2018-09-03 00:00:00.000', '2018-09-03 00:00:00.000', 2, 0, 0, 0, 1);
INSERT INTO youyaboot.dbo.sys_admin_user (id, username, password, real_name, email, telephone, mobile_phone, address, create_time, update_time, role_id, account_non_expired, account_non_locked, credentials_non_expired, enabled) VALUES (7, 'magicalcoder', '6860c37b00ed4e444a7d2c6e8fb66886', '系统默认超级管理员账号', '', '', '', '', '2018-09-06 00:00:00.000', '2018-09-06 00:00:00.000', 1, 0, 0, 0, 1);
SET IDENTITY_INSERT youyaboot.dbo.sys_admin_user OFF

CREATE TABLE youyaboot.dbo.sys_global_permit_url
(
  id bigint PRIMARY KEY NOT NULL IDENTITY,
  permit_name varchar(200),
  backend_url_reg varchar(500),
  module_id bigint
);
SET IDENTITY_INSERT sys_global_permit_url ON
INSERT INTO youyaboot.dbo.sys_global_permit_url (id, permit_name, backend_url_reg, module_id) VALUES (2, '允许进入后台,使用有些通用接口（请勿删除）', '/admin/(rmp|page|common_file_rest).*', null);
INSERT INTO youyaboot.dbo.sys_global_permit_url (id, permit_name, backend_url_reg, module_id) VALUES (3, '通用获取权限相关接口（请勿删除）', '/admin/rest_rmp/(get_permission_list|get_module_list).*', null);
SET IDENTITY_INSERT sys_global_permit_url OFF

CREATE TABLE youyaboot.dbo.sys_log_admin_operate
(
  id bigint PRIMARY KEY NOT NULL IDENTITY,
  admin_user_id bigint,
  user_name varchar(200),
  table_name varchar(100),
  operate_type varchar(50),
  url varchar(100),
  primary_id_value varchar(100),
  form_body text,
  create_time datetime
);

CREATE TABLE youyaboot.dbo.sys_module
(
  id bigint PRIMARY KEY NOT NULL IDENTITY,
  module_name varchar(200),
  module_url varchar(200),
  module_category_id bigint,
  sort_num int,
  module_title varchar(50),
  if_show bit
);
SET IDENTITY_INSERT youyaboot.dbo.sys_module ON
INSERT INTO youyaboot.dbo.sys_module (id, module_name, module_url, module_category_id, sort_num, module_title, if_show) VALUES (1, 'goods', 'admin/page/goods/list', 1, 1, '商品管理', 1);
INSERT INTO youyaboot.dbo.sys_module (id, module_name, module_url, module_category_id, sort_num, module_title, if_show) VALUES (2, 'goods_category', 'admin/page/goods_category/list', 1, 2, '商品类别', 1);
INSERT INTO youyaboot.dbo.sys_module (id, module_name, module_url, module_category_id, sort_num, module_title, if_show) VALUES (4, 'sys_log_admin_operate', 'admin/page/sys_log_admin_operate/list', 3, 7, '系统日志', 1);
INSERT INTO youyaboot.dbo.sys_module (id, module_name, module_url, module_category_id, sort_num, module_title, if_show) VALUES (5, 'sys_module_category', 'admin/page/sys_module_category/list', 3, 1, '菜单管理', 1);
INSERT INTO youyaboot.dbo.sys_module (id, module_name, module_url, module_category_id, sort_num, module_title, if_show) VALUES (7, 'sys_admin_user', 'admin/page/sys_admin_user/list', 3, 3, '管理员', 1);
INSERT INTO youyaboot.dbo.sys_module (id, module_name, module_url, module_category_id, sort_num, module_title, if_show) VALUES (8, 'sys_global_permit_url', 'admin/page/sys_global_permit_url/list', 3, 6, '全局地址过滤', 1);
INSERT INTO youyaboot.dbo.sys_module (id, module_name, module_url, module_category_id, sort_num, module_title, if_show) VALUES (11, 'sys_role', 'admin/page/sys_role/list', 3, 2, '角色管理', 1);
INSERT INTO youyaboot.dbo.sys_module (id, module_name, module_url, module_category_id, sort_num, module_title, if_show) VALUES (17, 'sys', '', 3, -1, '系统表权限载体（请勿删除）', 0);
INSERT INTO youyaboot.dbo.sys_module (id, module_name, module_url, module_category_id, sort_num, module_title, if_show) VALUES (18, 'goods_img', 'admin/page/goods_img/list', 1, 4, '商品图集', 1);
INSERT INTO youyaboot.dbo.sys_module (id, module_name, module_url, module_category_id, sort_num, module_title, if_show) VALUES (19, 'goods_file', 'admin/page/goods_file/list', 1, 5, '商品附件', 1);
INSERT INTO youyaboot.dbo.sys_module (id, module_name, module_url, module_category_id, sort_num, module_title, if_show) VALUES (20, 'dict', 'admin/page/dict/list', 3, 10, '字典配置', 1);
SET IDENTITY_INSERT youyaboot.dbo.sys_module OFF

CREATE TABLE youyaboot.dbo.sys_module_category
(
  id bigint PRIMARY KEY NOT NULL IDENTITY,
  module_category_name varchar(20),
  sort_num int
);
SET IDENTITY_INSERT youyaboot.dbo.sys_module_category ON
INSERT INTO youyaboot.dbo.sys_module_category (id, module_category_name, sort_num) VALUES (1, '商品模块', 1);
INSERT INTO youyaboot.dbo.sys_module_category (id, module_category_name, sort_num) VALUES (3, '系统模块', 2);
SET IDENTITY_INSERT youyaboot.dbo.sys_module_category OFF

CREATE TABLE youyaboot.dbo.sys_permission
(
  id bigint PRIMARY KEY NOT NULL IDENTITY,
  permission_name varchar(50),
  filter_platform varchar(50),
  backend_url_reg varchar(200),
  front_dom varchar(1024),
  front_action varchar(50),
  module_id bigint
);
SET IDENTITY_INSERT youyaboot.dbo.sys_permission ON
INSERT INTO youyaboot.dbo.sys_permission (id, permission_name, filter_platform, backend_url_reg, front_dom, front_action, module_id) VALUES (2, '查询权限', 'backend', '/admin/goods_category_rest/.*', '', '', 2);
INSERT INTO youyaboot.dbo.sys_permission (id, permission_name, filter_platform, backend_url_reg, front_dom, front_action, module_id) VALUES (13, '全类型测试', 'backend', '/admin/all_type_rest/.*', null, '', 14);
INSERT INTO youyaboot.dbo.sys_permission (id, permission_name, filter_platform, backend_url_reg, front_dom, front_action, module_id) VALUES (15, '商品模块保存权限', 'front_backend', '/admin/goods_rest/(update|save).*', '.security_edit_form_operate_save', 'show', 1);
INSERT INTO youyaboot.dbo.sys_permission (id, permission_name, filter_platform, backend_url_reg, front_dom, front_action, module_id) VALUES (17, '商品模块添加按钮', 'front', '', '.security_list_query_operate_add_news', 'show', 1);
INSERT INTO youyaboot.dbo.sys_permission (id, permission_name, filter_platform, backend_url_reg, front_dom, front_action, module_id) VALUES (18, '商品模块(批量)删除权限', 'front_backend', '/admin/goods_rest/(delete|batch_delete).*', '.security_list_table_operate_del,.security_list_query_operate_del_all', 'show', 1);
INSERT INTO youyaboot.dbo.sys_permission (id, permission_name, filter_platform, backend_url_reg, front_dom, front_action, module_id) VALUES (19, '商品模块查询权限', 'backend', '/admin/goods_rest/(search|page|get).*', '', '', 1);
INSERT INTO youyaboot.dbo.sys_permission (id, permission_name, filter_platform, backend_url_reg, front_dom, front_action, module_id) VALUES (21, '所有系统表的所有权限(请勿删除)', 'backend', '/admin/(rest_rmp|sys_\w+_rest)/.*', '', 'show', 17);
INSERT INTO youyaboot.dbo.sys_permission (id, permission_name, filter_platform, backend_url_reg, front_dom, front_action, module_id) VALUES (22, '测试商品模块disabled', 'front', '', '.security_edit_form_param_goodsName,.security_edit_form_param_imgSrc,.security_edit_form_param_publishStatus,.security_edit_form_param_goodsStatus,.security_edit_form_param_price,.security_edit_form_param_shortBrief,.security_edit_form_param_goodsDescription,.security_edit_form_param_createTime,.security_edit_form_param_goodsCategoryId', 'show', 1);
INSERT INTO youyaboot.dbo.sys_permission (id, permission_name, filter_platform, backend_url_reg, front_dom, front_action, module_id) VALUES (23, '商品图集全部权限', 'front_backend', '/admin/goods_img_rest/.*', '', 'show', 18);
INSERT INTO youyaboot.dbo.sys_permission (id, permission_name, filter_platform, backend_url_reg, front_dom, front_action, module_id) VALUES (24, '商品附件全部权限', 'front_backend', '/admin/goods_file_rest/.*', '', 'show', 19);
INSERT INTO youyaboot.dbo.sys_permission (id, permission_name, filter_platform, backend_url_reg, front_dom, front_action, module_id) VALUES (25, '字典全部权限', 'front_backend', '/admin/dict_rest/.*', '', 'show', 20);
SET IDENTITY_INSERT youyaboot.dbo.sys_permission OFF

CREATE TABLE youyaboot.dbo.sys_role
(
  id bigint PRIMARY KEY NOT NULL IDENTITY,
  role_name varchar(20) DEFAULT ''
);
SET IDENTITY_INSERT youyaboot.dbo.sys_role ON
INSERT INTO youyaboot.dbo.sys_role (id, role_name) VALUES (1, '超级管理员');
INSERT INTO youyaboot.dbo.sys_role (id, role_name) VALUES (2, '测试账号');
SET IDENTITY_INSERT youyaboot.dbo.sys_role OFF

CREATE TABLE youyaboot.dbo.sys_role_module_permission
(
  id bigint PRIMARY KEY NOT NULL IDENTITY,
  role_id bigint,
  module_id bigint,
  permission_id bigint
);
SET IDENTITY_INSERT youyaboot.dbo.sys_role_module_permission ON
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (573, 2, 1, 15);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (574, 2, 1, 17);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (575, 2, 1, 18);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (576, 2, 1, 19);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (577, 2, 1, 22);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (578, 2, 18, null);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (579, 2, 19, null);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (580, 1, 1, 15);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (581, 1, 1, 17);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (582, 1, 1, 18);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (583, 1, 1, 19);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (584, 1, 1, 22);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (585, 1, 2, 2);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (586, 1, 4, null);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (587, 1, 5, null);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (588, 1, 7, null);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (589, 1, 8, null);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (590, 1, 11, null);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (591, 1, 17, 21);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (592, 1, 18, 23);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (593, 1, 19, 24);
INSERT INTO youyaboot.dbo.sys_role_module_permission (id, role_id, module_id, permission_id) VALUES (594, 1, 20, 25);
SET IDENTITY_INSERT youyaboot.dbo.sys_role_module_permission OFF
